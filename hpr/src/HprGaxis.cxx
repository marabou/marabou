#include "TFrame.h"
#include "TMath.h"
#include "TRegexp.h"
#include "HprGaxis.h"
#include <iostream>

ClassImp(HprGaxis)
/*! \class HprGraph
\brief HprGaxis (TGaxis) keeps position during zooming.

`HprGaxis` is a `TGaxis` with the extra feature that keeps its position
within a frame of a pad during zooming.
The first argument is a pointer to the pad the rest are as for TGaxis.
Remark concerning `TGaxis`:
Option "W" draws a grid at positions of the primary tick marks.
"gridlength" is in NDC units of the pad (canvas), the sign assumes
the axis on left(Y) or bottom(X).
Example: Y-axis on right, pad margins 0.1, then gridlength = -.8 
draws gridlines in complete frame.
*/


HprGaxis::HprGaxis(TPad * canvas, Double_t xmin, Double_t ymin, Double_t xmax, Double_t ymax,
               Double_t wmin, Double_t wmax, Int_t ndiv , 
               Option_t* chopt, Double_t gridlength): 
               TGaxis(xmin, ymin, xmax, ymax, wmin, wmax,ndiv, chopt,gridlength)
{
   fAxisOffset = 0;
   fRatio = 1;
   fWhere = 1;
   if (xmin == xmax) fWhere = 2; // Y axis
   fPad = canvas;
   fFrameX1 = fPad->GetFrame()->GetX1();
   fFrameX2 = fPad->GetFrame()->GetX2();
   fFrameY1 = fPad->GetFrame()->GetY1();
   fFrameY2 = fPad->GetFrame()->GetY2();
   fLogx = fPad->GetLogx();
   fLogy = fPad->GetLogy();
   if (fWhere == 1) {
      fAxisOffset = (fFrameY2 - ymax)/ ( fFrameY2 - fFrameY1);
      fRatio = (fWmax - fWmin) / ( fFrameX2 - fFrameX1);
   } else if (fWhere == 2) {
      fAxisOffset = (fFrameX2 - xmax)/ ( fFrameX2 - fFrameX1);
      fRatio = (fWmax - fWmin) / ( fFrameY2 - fFrameY1);
   }  
   TQObject::Connect("TPad", "Modified()",
                     "HprGaxis", this, "HandlePadModified()");
   fTimer = new TTimer();
   fTimer->Connect("Timeout()", "HprGaxis", this, "ReDoAxis()");
   fSkipNextNotify = 0;
};
//__________________________________________________________________

void HprGaxis::ReDoAxis()
{  
   // anything changed??
   if ( (fFrameX1 == fPad->GetFrame()->GetX1() && fFrameX2 == fPad->GetFrame()->GetX2()
      && fFrameY1 == fPad->GetFrame()->GetY1() && fFrameY2 == fPad->GetFrame()->GetY2())
      && fPad->GetLogy() == fLogy &&  fPad->GetLogx() == fLogx ) {
      return;
   }
   Double_t ledge = 0, uedge = 0;
   Double_t x, y, x1=0, y1=0, x2=1, y2=1;
   TRegexp loG("G");
   HprGaxis * a = this;
   x1 = fPad->GetFrame()->GetX1();
   x2 = fPad->GetFrame()->GetX2();
   y1 = fPad->GetFrame()->GetY1();
   y2 = fPad->GetFrame()->GetY2();
   Bool_t log_axis = kFALSE;
   // X axis
   if ( a->GetWhere() == 1 ) {
      if ( fPad->GetLogx() ) {
         x1 = TMath::Power(10, x1);
         x2 = TMath::Power(10, x2);
         log_axis = kTRUE;
      }
      ledge = fWmin + (x1 - fFrameX1) * a->GetRatio();
      uedge = ledge + (x2 - x1) * a->GetRatio();
      y = y2 - (y2 - y1) * fAxisOffset;
      if (fPad->GetLogy() ) {
         y = TMath::Power(10, y);
      }
      y1 = y2 = y;
   // Y axis
   } else if ( a->GetWhere() == 2 ) {
      if ( fPad->GetLogy() ) {
         y1 = TMath::Power(10, y1);
         y2 = TMath::Power(10, y2);
         log_axis = kTRUE;
      }
      ledge = fWmin + (y1 - fFrameY1) * a->GetRatio();
      uedge = ledge + (y2 - y1) * a->GetRatio();
      x = x2 - (x2 - x1) * fAxisOffset;
      if ( fPad->GetLogx() ) {
         x = TMath::Power(10, x);
      }
      x1 = x2 = x;
   }
   TString opt(a->GetOption());
   if ( log_axis ) {
      if ( !opt.Contains("G") ) {
         opt += "G";
         a->SetOption(opt);
      }
   } else {
      if ( opt.Contains("G") ) {
         opt(loG) =  "";
         a->SetOption(opt);
      }
   }
   a->SetX1(x1);
   a->SetX2(x2);
   a->SetY1(y1);
   a->SetY2(y2);
   a->SetWmin(ledge);
   a->SetWmax(uedge);
   fSkipNextNotify = 1;
   fPad->Modified();
   fPad->Update();
   fFrameX1 = fPad->GetFrame()->GetX1();
   fFrameX2 = fPad->GetFrame()->GetX2();
   fFrameY1 = fPad->GetFrame()->GetY1();
   fFrameY2 = fPad->GetFrame()->GetY2();
   if (fWhere == 1) {
      fRatio = (fWmax - fWmin) / ( fFrameX2 - fFrameX1);
   } else if (fWhere == 2) {
      fRatio = (fWmax - fWmin) / ( fFrameY2 - fFrameY1);
   }  
   fLogx = fPad->GetLogx();
   fLogy = fPad->GetLogy();
}
//__________________________________________________________________

void HprGaxis::HandlePadModified()
{
   if (gPad == fPad) {
      if ( fSkipNextNotify == 0 ) {
         fTimer->Start(20,kTRUE);
      }
      fSkipNextNotify = 0;
   }
}
