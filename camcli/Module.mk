# Module.mk for camcli module
# Copyright (c) 2000 Rene Brun and Fons Rademakers
#
# Author: Otto Schaile

MODDIR       := camcli
MODDIRS      := $(MODDIR)/src
MODDIRI      := $(MODDIR)/inc

CAMCLIDIR      := $(MODDIR)
CAMCLIDIRS     := $(CAMCLIDIR)/src
CAMCLIDIRI     := $(CAMCLIDIR)/inc

##### lib #####
CAMCLIH        := $(filter-out $(MODDIRI)/LinkDef%,$(wildcard $(MODDIRI)/*.h))
CAMCLIS        := $(filter-out $(MODDIRS)/G__%,$(wildcard $(MODDIRS)/*.c))
CAMCLIO        := $(CAMCLIS:.c=.o)
# $(info  ####### CAMCLIS = $(CAMCLIS))
CAMCLIDEP      := $(CAMCLIO:.o=.d)

CAMCLILIB      := $(LPATH)/libEsoneClient.$(SOEXT)
CFLAGS	+=	-D_NO_PROTOS_
# SUN rpc moved at some time (Ubuntu 21.04?) to "tirpc"
RPCLIB :=  $(shell if [ -e $(TIRPC) ] ; then  echo /lib/x86_64-linux-gnu/libtirpc.so.3 ; fi)
TIRPC		:=	/usr/include/tirpc
TIRPCI	:= -I$(TIRPC) -I$(TIRPC)/rpc
#CFLAGS := $(shell echo $(CFLAGS) ; if [ -e $(TIRPC) ] ; then  echo $(TIRPCI) ; fi)

# used in the main Makefile
ALLHDRS     += $(patsubst $(MODDIRI)/%.h,include/%.h,$(CAMCLIH))
ALLLIBS     += $(CAMCLILIB)

# include all dependency files
INCLUDEFILES += $(CAMCLIDEP)

##### local rules #####

include/%.h:    $(CAMCLIDIRI)/%.h
		cp $< $@

$(CAMCLILIB):     $(CAMCLIDO) $(CAMCLIO)
		@$(MAKELIB) $(PLATFORM) $(LD) "$(LDFLAGS)" \
		   "$(SOFLAGS)" libEsoneClient.$(SOEXT) $@ "$(CAMCLIO)" \
		   "$(RPCLIB)"
		   
#$(CAMCLIDO):     $(CAMCLIDS)
camcli/src/%.o:    camcli/src/%.c 
#		@echo CAMCLID############ $<  $@
		gcc $(NOOPT) $(CFLAGS) $(TIRPCI) -I. -o $@ -c $<

all-camcli:       $(CAMCLILIB)

clean-camcli:
		@rm -f $(CAMCLIO) $(CAMCLIDO)

clean::         clean-camcli

distclean-camcli: clean-camcli
		@rm -f $(CAMCLIDEP) $(CAMCLIDS) $(CAMCLIDH) $(CAMCLILIB)

distclean::     distclean-camcli
